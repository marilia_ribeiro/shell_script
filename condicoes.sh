#!/bin/bash

# Estrutura condicional IF, ELIF, ELSE, FI

NOME="Marilia Ribeiro da Silva"
IDADE=23

if [ $IDADE -lt 18 ]; then
	echo "Você é menor de idade"
else
	echo "Você é maior de idade"
fi


echo -n "Qual o seu sexo: M/F?"; read SEXO

# o usuário digitou alguma coisa não nula?
if [ -z $SEXO ] ; then
	echo "Por favor informe seu sexo."

# verifica se o usuário digitou 'M' ou 'F'
elif [ $SEXO != "M" ] && [ $SEXO != "F" ]; then
	echo "Informe apenas 'M' ou 'F'"
	echo "Por favor repita a operação."
	exit 0 #Saia sem erro

# se o usuário digitou 'M'
elif [ $SEXO == "M" ] ; then
	echo "Você informou que é do sexo masculino."
# se o usuário digitou 'F'
elif [ $SEXO == "F" ]; then
	echo "Você informou que é do sexo feminino."
else
	echo "Não reconheci sua resposta"
fi
