#!/bin/bash
#
# Laço de reperição - while

NUM=1

# enquanto NUM menor ou igual a 10
while [ $NUM -le 10 ]; do
	echo $NUM
	NUM=$[ $NUM + 1 ]
done
