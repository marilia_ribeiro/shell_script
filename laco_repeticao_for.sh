#!/bin/bash
#
# Laço de repetição - For
# Estrutura 'seq' pode ser utilizado com o for

# usa resultado da instrição 'seq' e itera sobre casa opção
for i in $(seq 10)
do
	echo -n " $i ,"
done

echo


dir="/var/log"
#lista_arquivos=$(ls /var/log)
lista_arquivos=$(ls $dir)

for f in ${lista_arquivos}
do
	echo "$f "
done
