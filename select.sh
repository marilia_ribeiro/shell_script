#!/bin/bash

clear
OPCAO="listar limpar sair"

select OPT in $OPCAO
do
	echo $OPT
	if [ $OPT == "listar" ] ; then
		echo "Listando arquivos"
		ls -lh
		exit
	elif [ $OPT == "limpar" ]; then
		echo "Limpando"
		sleep 2
		clear
		exit
	else
		echo "Saindo ..."
		sleep 2
		exit
	fi
done
