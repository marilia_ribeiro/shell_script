#!/bin/bash
#
# Testando permissões de arquivos

ESTE_SCRIPT=$0

echo "Propriedades deste arquivo"
echo "Nome do arquivo: $ESTE_SCRIPT"
echo

[ -r $ESTE_SCRIPT ] && echo "Este arquivo tem permissão de leitura"
[ -w $ESTE_SCRIPT ] && echo "Este arquivo tem permissão de gravação"
[ -x $ESTE_SCRIPT ] && echo "Este arquivo é autoexecutável"
[ -f $ESTE_SCRIPT ] && echo "Este arquivo é um arquivo regular"
[ -e $ESTE_SCRIPT ] && echo "Este arquivo existe"
[ -d $ESTE_SCRIPT ] && echo "Este arquivo é um ditetório"

