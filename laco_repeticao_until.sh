#!/bin/bash
#
# Laço de repetição - until

NUM=1

# until funciona do mesmo jeito que while, exceto pelo fato de que o teste é feito ao final do loop

# Faça até que NUM seja maior que 10
until [ $NUM -gt 10 ]; do
	echo $NUM
	NUM=$[ $NUM + 1 ]
done

