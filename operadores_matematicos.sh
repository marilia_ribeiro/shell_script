#!/bin/bash
#

clear

N1=5
N2=2
SOMA=$[ $N1 + $N2 ]
SUBTRACAO=$[ $N1 - $N2 ]
MULTIPLICACAO=$[ $N1 * $N2 ]
DIVISAO=$[ $N1 / $N2 ]
MODULO=$[ $N1 % $N2 ]
EXPO=$[ $N1 ** $N2 ]

echo "============================="
echo "Números informados: $N1 - $N2"
echo "============================="
echo "Soma: $SOMA"
echo "Subtração: $SUBTRACAO"
echo "Multiplicação: $MULTIPLICACAO"
echo "Divisão Inteira: $MODULO"
echo "Módulo/Resto Divisão: $MODULO"
echo "Exponencial: $EXPO"

