#!/bin/bash

# Estrutura case , esac

clear

echo "Qual o seu time preferido?"
echo "Escolha uma das opções abaixo:"
echo
echo "1 - Flamengo"
echo "2 - Fluminense"
echo "3 - Vasco"
echo "4 - Botafogo"
echo "5 - Palmeiras"
echo "6 - São Paulo"
echo "0 - Nenhuma acima"

echo -n "Opção >> "; read OPCAO
echo
echo -n "Seu time é "

# estrutura case - cada bloco deve ser finalixado com ';;'
case $OPCAO in
	1) echo "Flamengo" ;;
	2) echo "Fluminense" ;;
	3) echo "Vasco" ;;
	4) echo "Botafogo" ;;
	5|6)  echo "Time de S. Paulo não vale ..." ;;
	0) echo "Meu time não está listado" ;;
	*) echo "A opçaõ escolhida não está listada" ;;
esac
