#!/bin/bash
#

# Testes especiais:
# -z => não nulo
# -n => é nulo
# = => igual
# != => diferente


VAR1=""

if [ -z $VAR1 ]; then
	echo "Variável com conteúdo nulo"
else
	echo "Conteúdo da variável: $VAR1"
fi

VAR2="Abobora"

if [ -n $VAR2 ]; then
	echo "A variável VAR2 tem valor: $VAR2"
else
	echo "A variável VAR2 tem conteúdo nulo"
fi

VAR3="Marilia"

if [ $VAR3 == $VAR2 ]; then
	echo "A variável $VAR3 tem conteúdo igual a variável $VAR2"
else
	echo "A variável $VAR3 tem conteúdo diferente da variável $VAR2"
fi


N1=5
N2=10

if  [ $N1 -eq $N2 ]; then
	echo "$N1 é igual a $N2"
fi

if [ $N1 -gt $N2 ]; then
	echo "$N1 é maior que $N2"
fi

if [ $N1 -lt $N2 ]; then
	echo "$N1 é menor que $N2"
fi

if [ $N1 -ge $N2 ]; then
	echo "$N1 é maior ou igual a $N2"
fi

if [ $N1 -le $N2 ]; then
	echo "$N1 é menor ou igual a $N2"
fi

if [ $N1 -ne $N2 ]; then
	echo "$N1 é diferente de $N2"
fi
